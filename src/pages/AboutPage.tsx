import React from "react";
import { useHistory } from "react-router-dom";

const AboutPage: React.FC = () => {
  const history = useHistory();

  return (
    <>
      <h1>Info</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque animi
        eius quae enim earum perspiciatis at aliquid omnis sint laborum.
      </p>
      <button className="btn" onClick={() => history.push("/")}>
        Back to the list
      </button>
    </>
  );
};

export default AboutPage;
