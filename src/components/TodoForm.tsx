import React, { useState } from "react";

type TodoFormProps = {
  onAdd(title: string): void;
};

const TodoForm: React.FC<TodoFormProps> = ({ onAdd }) => {
  const [title, setTitle] = useState<string>("");

  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(e?.target.value);
  };

  const keyPressHandler = (e: React.KeyboardEvent) => {
    if (e.key === "Enter") {
      onAdd(title);
      setTitle("");
    }
  };

  return (
    <div className="input-field">
      <input
        autoComplete="off"
        type="text"
        id="title"
        placeholder="Ented Todo title"
        onChange={changeHandler}
        onKeyPress={keyPressHandler}
        value={title}
      />
      <label htmlFor="title" className="active">
        Ented Todo title
      </label>
    </div>
  );
};

export default TodoForm;
