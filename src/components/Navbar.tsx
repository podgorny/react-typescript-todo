import React from "react";
import { NavLink, useHistory } from "react-router-dom";

const Navbar: React.FC = () => {
  const history = useHistory();

  const getNavLinkClass = (path: string) => {
    return history.location.pathname === path ? "active" : "";
  };

  return (
    <nav>
      <div className="nav-wrapper px1">
        <NavLink to="/" className="brand-logo flow-text">
          <span className="flow-text">React & Typescript</span>
        </NavLink>
        <ul className="right hide-on-med-and-down">
          <li className={getNavLinkClass("/")}>
            <NavLink to="/">Todo list</NavLink>
          </li>
          <li className={getNavLinkClass("/about")}>
            <NavLink to="/about">Info</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
